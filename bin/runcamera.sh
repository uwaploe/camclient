#!/bin/bash
#
# Run a camclient command file.
#

: ${CAMHD_ENCODE=hdcam-local}
: ${SMTP_RELAY="10.20.1.9"}
: ${LOGDIR=$HOME/logs}
# Semicolon separated list of recipients for email alerts
# from camclient
: ${CAMHD_ALERT="mikek@apl.uw.edu;mikeh@apl.washington.edu"}

file="$1"
[[ "$file" ]] || {
    echo "Usage: $(basename $0) cmdfile" 1>&2
    exit 1
}

export PATH=$HOME/.local/bin:$HOME/bin:$PATH

set -e

# The command file names are relative to $BATCHDIR if set
[[ -n $BATCHDIR ]] && cd $BATCHDIR

[[ -r "$file" ]] || {
    echo "$file: command file not found" 1>&2
    exit 1
}

camclient "load $file" "quit" 1> $LOGDIR/CAMHDA301-$(date +'%Y%m%dT%H%M')00Z.log 2>&1

# Power cycle the camera
ssh -t $CAMHD_ENCODE \
    "source ~/.venvs/hdcam/bin/activate;ncdtest.py 'clear 0' quit" 1> /dev/null 2>&1
sleep 5
ssh -t $CAMHD_ENCODE \
    "source ~/.venvs/hdcam/bin/activate;ncdtest.py 'set 0' quit" 1> /dev/null 2>&1
