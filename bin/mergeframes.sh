#!/usr/bin/env bash
#
# Use ffmpeg to merge uncompressed YUV video frame files into an
# MP4 video file. The frame files are then deleted.
#
# This technique is described on the Ultragrid Wiki:
#
# https://github.com/CESNET/UltraGrid/wiki/Recording-and-Playback
#


# Check for required utilities
set -e
type ffmpeg parallel >&2

outfile="$1"
[[ -z $outfile ]] && {
    echo "Output file not specified" 1>&2
    echo "Usage: ${0##*/} outfile [framedir]" 1>&2
    exit 1
}

framedir="$2"
[[ -d "$framedir" ]] && {
    outfile="$(pwd)/$outfile"
    cd "$framedir"
}

# Rename the frame image files, ffmpeg expects the ".Y" extension
ls | grep '\.yuv$' | sed 's/\.yuv$//' | parallel mv {}.yuv {}.Y

# Create the video file
ffmpeg -pix_fmt uyvy422 -s 1920x1080 -i %08d.Y -codec:v libx264 "$outfile"

# Remove the frame image files
rm -f *.Y
