"""
.. module:: camhd.comm
     :platform: Any
     :synopsis: ZeroMq communication utility functions
"""
import zmq
from typing import List, Tuple, Generator


def send_msg(sock: zmq.Socket, msg: List[bytes], timeout: int = 2500) -> List[bytes]:
    """
    Send a multi-part message to a ZeroMQ endpoint and return the multi-part
    response.

    :param sock: socket object
    :type sock: zmq.core.socket.Socket
    :param msg: message contents
    :param timeout: maximum time (ms) to wait for a response
    :return: multi-part response
    :rtype: tuple or ``None``
    """
    result = []
    poll = zmq.Poller()
    poll.register(sock)
    sock.send_multipart(msg)
    socks = dict(poll.poll(timeout))
    if socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
    poll.unregister(sock)
    return result


def subscription(sock: zmq.Socket,
                 sentinel: Tuple[bytes],
                 timeout: int = 10000) -> Generator[List[bytes], None, None]:
    """
    Generator to return messages from a SUB socket until *sentinel*
    is seen or *timeout* expires.
    """
    poll = zmq.Poller()
    poll.register(sock)
    socks = dict(poll.poll(timeout))
    while socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
        yield result
        if result[0] not in sentinel:
            socks = dict(poll.poll(timeout))
        else:
            break
