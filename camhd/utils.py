"""
.. module:: camhd.utils
     :platform: Any
     :synopsis: utility functions
"""
import os
import fcntl
from ast import literal_eval
from typing import Any, Dict
from contextlib import contextmanager
import smtplib
from email.message import EmailMessage


def args_to_dict(args: str, do_eval: bool = True) -> Dict[str, Any]:
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary.

    >>> d = args_to_dict('foo=bar baz=1,2,3 x= y').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar'), ('x', ''), ('y', None)]
    """
    def maybe_eval(s):
        if do_eval:
            try:
                return literal_eval(s)
            except (ValueError, SyntaxError):
                return s
        else:
            return s
    params: Dict[str, Any] = {}
    for arg in [a.split('=', 1) for a in args.split()]:
        try:
            name, val = arg
        except ValueError:
            name = arg[0]
            params[name] = None
            continue
        if val and (',' in val):
            params[name] = [maybe_eval(v) for v in val.split(',')]
        else:
            params[name] = maybe_eval(val)
    return params


@contextmanager
def lock_file(path):
    try:
        lockf = open(path, 'w')
        fcntl.flock(lockf, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        raise SystemExit("file lock failed")
    try:
        yield lockf
    finally:
        fcntl.flock(lockf, fcntl.LOCK_UN)
        lockf.close()
        os.unlink(path)


def send_message(relay, subject, body, sender, addrs):
    """
    Send a message to a list email addresses.

    @param subject: message subject.
    @param body: message contents.
    @param sender: message sender.
    @param addrs: list of email addresses.
    """
    msg = EmailMessage()
    msg.set_content(body)
    msg['From'] = sender
    msg['To'] = ", ".join(addrs)
    msg['Subject'] = subject
    s = smtplib.SMTP(relay)
    s.send_message(msg)
    s.quit()
