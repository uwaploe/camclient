#!/usr/bin/env python3

import os
import sys
from optparse import OptionParser
from camhd.client import HdcamApp
from camhd.utils import lock_file, send_message


def main():
    abort_msg = """
    Warning, CAMHD client aborted with code:

      '{}'
    """
    # Create an OptionParser just to produce a usage message, HdcamApp
    # actually processes the command line.
    parser = OptionParser(usage=__doc__)
    parser.parse_args()

    relay = os.environ.get("SMTP_RELAY", "10.20.1.9")
    addrs = os.environ.get("CAMHD_ALERT", "").split(";")

    # Line buffer stdout and stderr
    sys.stdout.reconfigure(line_buffering=True)
    sys.stderr.reconfigure(line_buffering=True)

    obj = HdcamApp()
    try:
        with lock_file("/var/lock/camhd.lock"):
            obj.cmdloop()
        return 0
    except SystemExit as e:
        print(e.code, file=sys.stderr)
        if len(addrs) > 0:
            send_message(relay,
                         "CAMHD error",
                         abort_msg.format(e.code),
                         "camhd-noreply@uw.edu",
                         addrs)
        return e.code


if __name__ == '__main__':
    sys.exit(main())
