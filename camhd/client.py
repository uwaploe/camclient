"""
.. module:: camhd.client
     :platform: Any
     :synopsis: Interactive client for CAM-HD
"""
import cmd2
import time
import zmq
import json
import subprocess
import os
import signal
import shlex
from datetime import datetime, timezone
from typing import List
from functools import reduce
from . import comm, utils, hyperdeck


# Template for Ultragrid command
UV_TEMPLATE = 'uv --tool uv -d {sink} -m {mtu:d} {endpoint}'


class HdcamApp(cmd2.Cmd):
    """
    Simple command interpreter for accessing the CAMHD server

    Commands (type help <topic>)
    =================================================
    open close start stop lookat lights camera adread
    hd time twait sleep quit
    """
    std_prompt = 'HDCAM> '
    rec_prompt = '(rec)HDCAM> '
    socket = None
    ctx = None
    host = ''
    proc = None
    timeout = 20
    epoch = 0
    reconns = 0
    logf = None
    # Map the file format names used by the Hyperdeck
    # to something easier to remember and type.
    hd_formats = {
        'raw': 'QuickTimeUncompressed',
        'hq': 'QuickTimeProResHq'
    }

    def log_event(self, event: str, data: str = ''):
        if self.logf:
            t = time.time()
            secs, usecs = divmod(int(t * 1000000), 1000000)
            self.logf.write('{0},{1},{2},"{3}"\n'.format(secs, usecs,
                                                         event, data))
            self.logf.flush()

    def preloop(self):
        self.prompt = self.std_prompt

    def _open(self, host: str):
        if self.socket is not None:
            self._close()
        if self.ctx is None:
            self.ctx = zmq.Context()
        self.host = host
        self.socket = self.ctx.socket(zmq.REQ)
        self.socket.connect('tcp://{0}:5500'.format(host))

    def _close(self):
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.close()
        self.socket = None

    def _stop_proc(self):
        if self.proc:
            os.killpg(os.getpgid(self.proc.pid), signal.SIGTERM)
            self.poutput('Waiting for Ultragrid process to exit...\n')
            try:
                self.proc.wait(timeout=3)
            except subprocess.TimeoutExpired:
                self.poutput('WARNING: Ultragrid process did not terminate')
                os.killpg(os.getpgid(self.proc.pid), signal.SIGKILL)
            self.proc = None

    def _send(self, msg: List[bytes]) -> List[bytes]:
        assert self.socket is not None
        result = comm.send_msg(self.socket, msg, self.timeout * 1000)
        if len(result) == 0:
            self.perror('No response from server. Reconnecting ...\n')
            self._close()
            self._open(self.host)
            self.reconns += 1
            result = [b'TIMEOUT', b'']
        return result

    def do_help(self, arg: str):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            if self.__doc__:
                self.poutput(self.__doc__)

    def do_open(self, args):
        """open HOST: open a connection to the CAMHD host."""
        if not args:
            self.perror('You must specify a hostname\n')
        else:
            self._open(args)

    def do_close(self, args):
        """close: close the network connection"""
        assert self.socket is not None
        self._close()

    def do_sleep(self, args):
        """sleep SECS: pause for SECS seconds."""
        if args:
            time.sleep(float(args))
        else:
            self.perror('missing argument')

    def do_twait(self, args):
        """twait [HH:[MM:[SS]]]: pause until absolute time since stream start."""
        if args:
            t = [int(a, 10) for a in args.split(':')]
            secs = reduce(lambda x, y: x * 60 + y, t, 0)
            try:
                time.sleep(self.epoch + secs - time.time())
            except IOError:
                self.perror('Warning: wait time is in the past\n')
        else:
            self.perror('missing argument')

    def do_start(self, args):
        """start endpoint=HOST [PARAM=VALUE ...]: start video streaming."""
        assert self.socket is not None
        if self.proc is not None:
            self.do_stop()

        params = utils.args_to_dict(args, do_eval=False)
        resp, contents = self._send([
            b'START',
            json.dumps(params).encode('us-ascii')
        ])
        self.poutput('{0}: {1}'.format(
            resp,
            contents
        ))
        if resp is None:
            self.perror("Invalid response from server, aborting\n")
            raise SystemExit("Invalid server response")

        if resp != b'ERROR':
            self.pfeedback('Starting local Ultragrid process')
            uv_cmd = UV_TEMPLATE.format(endpoint=self.host,
                                        mtu=int(params.get('mtu', 8500)),
                                        sink=params.get('sink', 'decklink:device=0'))
            outf = open('/dev/null', 'w')
            try:
                self.proc = subprocess.Popen(
                    shlex.split(uv_cmd),
                    stdout=outf,
                    stderr=subprocess.STDOUT,
                    start_new_session=True
                )
                self.poutput('Ultragrid started: pid={0:d}'.format(self.proc.pid))
            except (OSError, ValueError) as e:
                self.perror(str(e) + '\n')
            else:
                self.register_precmd_hook(self.check_proc)
                self.epoch = time.time()

    def check_proc(self,
                   data: cmd2.plugin.PrecommandData) -> cmd2.plugin.PrecommandData:
        """Verify that the Ultragrid process is still running. Abort if it's not """
        if self.proc is not None:
            rc = self.proc.poll()
            if rc is not None:
                self.perror('Ultragrid exited with code {0:d}\n'.format(rc))
                self.proc = None
                self.do_stop('')
                raise SystemExit(rc)
        return data

    def do_time(self, args):
        """time: display elapsed time since stream start."""
        if self.epoch == 0:
            self.poutput('0')
        else:
            dt = int(time.time() - self.epoch)
            h, rem = divmod(dt, 3600)
            m, s = divmod(rem, 60)
            self.poutput('{0:02d}:{1:02d}:{2:02d}'.format(h, m, s))

    def do_stop(self, args):
        """stop: stop video streaming."""
        assert self.socket is not None
        resp, contents = self._send([
            b'STOP',
            b''
        ])
        self.poutput('{0}: {1}'.format(
            resp,
            contents
        ))
        self._stop_proc()
        self.epoch = 0

    def do_lookat(self, args):
        """lookat [pan=ANGLE tilt=ANGLE speed=DEG/S]: get or set absolute pan/tilt."""
        assert self.socket is not None
        params = utils.args_to_dict(args)
        resp, contents = self._send([
            b'LOOKAT',
            json.dumps(params).encode('us-ascii')
        ])
        if args:
            sub = self.ctx.socket(zmq.SUB)
            sub.setsockopt(zmq.SUBSCRIBE, b'INMOTION')
            sub.setsockopt(zmq.SUBSCRIBE, b'STOPPED')
            sub.setsockopt(zmq.SUBSCRIBE, b'STALLED')
            sub.connect('tcp://{0}:5501'.format(self.host))
            for msg in comm.subscription(sub, (b'STOPPED', b'STALLED')):
                resp, contents = msg
                self.log_event('state', contents)
                self.poutput('{0}: {1}'.format(
                    resp,
                    contents
                ))
            sub.close()
        obj = json.loads(contents)
        if resp == 'ERROR':
            self.perror('ERROR: {0}'.format(contents))
        else:
            keys = ('pan', 'tilt', 'heading', 'pitch', 'pan_flag', 'tilt_flag')
            for k in keys:
                v = obj.get(k)
                if v is not None:
                    self.poutput('{0}: {1}'.format(k, v))

    def do_lights(self, args):
        """lights [INTENSITY1 [INTENSITY2]]: check or adjust light intensity."""
        assert self.socket is not None
        if args:
            vals = [int(a, 0) for a in args.split()]
            if len(vals) == 1:
                vals.append(vals[0])
            params = {'intensity': vals}
        else:
            params = {}
        resp, contents = self._send([
            b'LIGHTS',
            json.dumps(params).encode('us-ascii')
        ])
        self.poutput('{0}: {1}'.format(
            resp,
            contents
        ))

    def do_camera(self, args):
        """camera [laser=on|off zoom=STEPS]: check or adjust camera settings."""
        assert self.socket is not None
        params = utils.args_to_dict(args)
        resp, contents = self._send([
            b'CAMERA',
            json.dumps(params).encode('us-ascii')
        ])
        self.poutput('{0}: {1}'.format(
            resp,
            contents
        ))

    def do_adread(self, args):
        """adread: read A/D channels."""
        assert self.socket is not None
        params = utils.args_to_dict(args)
        resp, contents = self._send([
            b'ADREAD',
            json.dumps(params).encode('us-ascii')
        ])
        self.log_event('adc', contents)
        obj = json.loads(contents)
        if resp == b'ERROR':
            self.perror('ERROR: {0}'.format(obj))
        else:
            for row in obj['data']:
                self.poutput('{name}: {val:.3f} {units}'.format(**row))

    def help_hd(self):
        self.poutput("""
        hd open host=ADDR
          Initialize the connection to the Hyperdeck. This command
          must be sent before any subsequent 'hd' commands. The
          Hyperdeck hostname or IP must be supplied.

        hd rec [input=SDI] [fmt=QuickTimeUncompressed]
          Start recording a video clip.

        hd stop
          Stop the current recording.

        hd close
          Close the network connection to the Hyperdeck.
        """)

    def do_hd(self, args):
        params = utils.args_to_dict(args)
        if 'open' in params:
            host = params.get('host')
            if not host:
                self.perror('ERROR: bad/missing host name\n')
            else:
                try:
                    self.hd = hyperdeck.Hyperdeck(host)
                    self.hd.send_command('remote', enable='true')
                    self.hd.get_response()
                except Exception as e:
                    self.perror('ERROR: {0!r}'.format(e))
        else:
            if self.hd is None:
                self.perror('ERROR: run "hd open host=NAME" first\n')
                return
            if 'rec' in params:
                self.hd_configure(**params)
                self.start_recording()
            elif 'stop' in params:
                self.stop_recording()
            elif 'close' in params:
                self.stop_recording()
                self.hd = None
            else:
                self.poutput('Invalid argument: {0!r}'.format(args))

    def hd_configure(self, **kwds):
        """Change the Hyperdeck configuration."""
        if 'input' in kwds:
            self.hd.send_command('configuration',
                                 video_input=kwds['input'].upper())
            self.hd.get_response()
        if 'fmt' in kwds:
            fmt = self.hd_formats.get(kwds['fmt'], kwds['fmt'])
            self.hd.send_command('configuration',
                                 file_format=fmt)
            self.hd.get_response()

    def start_recording(self):
        """Start recording a video clip on the Hyperdeck"""
        if self.recmode:
            self.stop_recording()
        t = time.strftime('%Y%m%d_%H%M%S', time.gmtime())
        clip = 'clip_{0}_'.format(t)
        try:
            self.hd.send_command('configuration')
            code, resp = self.hd.get_response()
            self.poutput(resp)
            self.hd.send_command('record', name=clip)
            code, resp = self.hd.get_response()
            self.poutput(resp)
            filename = 'log_{0}.csv'.format(t)
            self.logf = open(filename, 'w')
            self.poutput('Logging to {0}'.format(filename))
        except Exception as e:
            self.perror('ERROR: {0!r}'.format(e))
        else:
            self.log_event('command', 'rec')
            self.recmode = True
            self.prompt = self.rec_prompt

    def stop_recording(self):
        """Stop the Hyperdeck recording."""
        if self.recmode:
            try:
                self.hd.send_command('stop')
                code, resp = self.hd.get_response()
                self.poutput(resp)
            except Exception as e:
                self.perror('ERROR: {0!r}'.format(e))
                self.hd = None
            self.logf.close()
            self.logf = None
            self.poutput('Logfile closed')
            self.prompt = self.std_prompt
            self.recmode = False

    def do_load(self, args):
        """load FILENAME: execute commands from a file."""
        if not args:
            self.perror('Missing filename\n')
        else:
            name = args.strip()
            try:
                with open(name, 'r') as f:
                    for line in f:
                        if line and (not line.startswith('#')):
                            t = datetime.now(timezone.utc)
                            self.poutput('{0} > {1}'.format(t.isoformat(), line.strip()))
                            stop = self.onecmd_plus_hooks(line)
                            if stop:
                                return True
            except IOError as e:
                self.perror(str(e))
