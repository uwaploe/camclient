try:
    from importlib import metadata # type: ignore
except ImportError:
    # Running on pre-3.8 Python; use importlib-metadata package
    import importlib_metadata as metadata # type: ignore

__version__ = metadata.version(__name__)
