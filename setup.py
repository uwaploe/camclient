from setuptools import setup


setup(name="camhd",
      use_scm_version={
          "root": ".",
          "relative_to": __file__,
          "local_scheme": "node-and-timestamp"
      },
      setup_requires=['setuptools_scm'],
      description="Client side software for the CAM-HD project",
      url="https://bitbucket.org/uwaploe/camclient",
      author="Michael Kenney",
      author_email="mikek@apl.uw.edu",
      license="MIT",
      packages=["camhd", "camhd.cli"],
      package_data={"camhd": ["py.typed"],
                    "camhd.cli": ["py.typed"]},
      install_requires=[
          'importlib-metadata ~= 1.0 ; python_version < "3.8"',
          'cmd2 >= 2.3.3',
          'pyzmq >= 20.0.0'
      ],
      python_requires="~=3.7",
      scripts=["bin/runcamera.sh", "bin/mergeframes.sh"],
      entry_points={
          "console_scripts": [
              "camclient=camhd.cli.camclient:main",
          ]
      },
      zip_safe=False)
