# CAM-HD Client Software

This repository contains the source code for a Python (v3) package which provides an interactive client to control CAM-HD. This software was previously part of the [pyhdcam package](https://bitbucket.org/uwaploe/rsn-pyhdcam/src/develop/) but has now been separated and migrated to Python3.

## Prerequisites

### Blackmagic Drivers

The latest drivers for the Blackmagic video capture/output board can be downloaded from the link below:

https://www.blackmagicdesign.com/support/family/capture-and-playback

Download the Desktop Video package (**not** the developer SDK) for Linux and unpack the compressed TAR archive. The archive contains installation documentation.

### Ultragrid

This is the video streaming software which can be downloaded from the link below. I suggest installing the AppImage rather than building from source.

http://www.ultragrid.cz/installation-and-running/

Be sure to following the Ultragrid [OS setup instructions](https://github.com/CESNET/UltraGrid/wiki/OS-Setup-UltraGrid) and make sure the Ultragrid executable (uv) is in the PATH.

### Python packages

``` shellsession
apt-get install python3-zmq python3-cmd2 python3-pip python3-setuptools-scm
```

## Installation

Download the most recent package source from the [Downloads](https://bitbucket.org/uwaploe/camclient/downloads/) section of this repository and install using `pip3`:

``` shellsession
pip3 install camhd-$VERSION.tar.gz
```

The `camclient` and `runcamera.sh` scripts will be installed in `$HOME/.local/bin` so be sure to add that directory to your PATH.

### Version Check

Run the command below to check the version of the the installed `camhd` package:

``` shellsession
python3 -c 'import camhd; print(camhd.__version__)'
```
