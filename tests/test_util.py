"""
Tests from camhd.utils
"""
import unittest
from camhd.utils import args_to_dict


class ArgsTestCase(unittest.TestCase):
    def test_args_eval(self):
        d = args_to_dict('foo=bar baz=1,2,3 x= y')
        self.assertEqual(d['foo'], 'bar')
        self.assertEqual(d['baz'], [1, 2, 3])
        self.assertEqual(d['x'], '')
        self.assertIsNone(d['y'])

    def test_args_noeval(self):
        d = args_to_dict('foo=0:mode=10:single x=42 y=hello', do_eval=False)
        self.assertEqual(d['foo'], '0:mode=10:single')
        self.assertEqual(d['x'], '42')
        self.assertEqual(d['y'], 'hello')


if __name__ == '__main__':
    unittest.main()
