"""
Tests from camhd.comm
"""
import zmq
import json
import unittest
import time
from datetime import datetime, timezone
from threading import Thread
from camhd.comm import send_msg, subscription
from typing import Generator, Tuple


def rep_server(ctx: zmq.Context):
    sock = ctx.socket(zmq.REP)
    sock.bind('tcp://*:5500')
    while True:
        cmd, params = sock.recv_multipart()
        sock.send_multipart([b'OK', params])
        if cmd == b'BYE':
            break


def publisher(ctx: zmq.Context,
              src: Generator[Tuple[bytes], None, None]):
    sock = ctx.socket(zmq.PUB)
    sock.bind('tcp://*:5501')
    for reply, contents in src:
        sock.send_multipart([reply, contents])


class MsgTestCase(unittest.TestCase):
    def setUp(self):
        self.ctx = zmq.Context()
        self.tid = Thread(target=rep_server, args=(self.ctx,))
        self.tid.start()
        self.sock = self.ctx.socket(zmq.REQ)
        self.sock.connect('tcp://localhost:5500')

    def tearDown(self):
        self.sock.send_multipart([b'BYE', b''])
        self.tid.join(timeout=1)

    def test_send_cmd(self):
        d = dict(zoom=5, laser="off")
        resp, params = send_msg(self.sock, [b'CAMERA',
                                            json.dumps(d).encode('us-ascii')])
        self.assertEqual(resp, b'OK')
        d2 = json.loads(params.decode('us-ascii'))
        self.assertEqual(d2['laser'], 'off')
        self.assertEqual(d2['zoom'], 5)


def msg_gen(n: int, interval: float = 0.5) -> Generator[Tuple[bytes, bytes], None, None]:
    for i in range(n):
        t = datetime.now(timezone.utc)
        yield (b'STATUS', json.dumps(dict(time=t.isoformat())).encode('us-ascii'))
        time.sleep(interval)
    yield (b'DONE', b'')


class SubTestCase(unittest.TestCase):
    def test_subscription(self):
        ctx = zmq.Context()
        sock = ctx.socket(zmq.SUB)
        sock.setsockopt(zmq.SUBSCRIBE, b'DONE')
        sock.setsockopt(zmq.SUBSCRIBE, b'STATUS')
        sock.connect('tcp://localhost:5501')
        tid = Thread(target=publisher, args=(ctx, msg_gen(5)))
        tid.start()
        for msg in subscription(sock, (b'DONE',), timeout=2000):
            pass
        sock.close()
        self.assertEqual(msg[0], b'DONE')
        tid.join(timeout=1)


if __name__ == '__main__':
    unittest.main()
